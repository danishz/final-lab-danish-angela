package com.example.gaedemo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShowItemsActivity extends Activity/*, AsyncTask<String, Void, List<String>> */{
	public static final String TAG = "ShowItemsActivity";
	//private EditText dataList = (EditText) findViewById(R.id.dataList);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// create showitems layout first.
		
		// Click on layout, New->Android XML File.
		// ResourceType should be Layout.
		// Add a EditText field in it showitems.xml
		setContentView(R.layout.showitems);
		
		EditText dataList = (EditText) findViewById(R.id.dataList);
		// Now create a method which get query your Google App Engine via HTTP GET.
		// Make sure that method gets the data via a new thread
		// or use async task.
		UpdateItemsTask uit = new UpdateItemsTask();
		uit.execute(MainActivity.ITEM_URI);
		// Update the TextView with itemsData.
		
	}
	

	 private class UpdateItemsTask extends AsyncTask<String, Void, List<String>> {
		 @Override
	     protected List<String> doInBackground(String... url) {
			 
	    	 HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet(url[0]);
				List<String> list = new ArrayList<String>();
				try {
					HttpResponse response = client.execute(request);
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					Log.d(TAG, data);
					JSONObject myjson;
					
					try {
						myjson = new JSONObject(data);
						JSONArray array = myjson.getJSONArray("data");
						for (int i = 0; i < array.length(); i++) {
							JSONObject obj = array.getJSONObject(i);
							list.add(obj.get("name").toString());
						}
						
					} catch (JSONException e) {

				    	Log.d(TAG, "Error in parsing JSON");
					}
					
				} catch (ClientProtocolException e) {

			    	Log.d(TAG, "ClientProtocolException while trying to connect to GAE");
				} catch (IOException e) {

					Log.d(TAG, "IOException while trying to connect to GAE");
				}
	         return list;
	     }

	     protected void onPostExecute(List<String> list) {
	    	 Context context = getBaseContext();
	    	 LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	 View view = mInflater.inflate(R.layout.showproducts, null);
	    	 LinearLayout layout = (LinearLayout) view;
	    	 
	    	 // Loop through the list and create a new TextView for each item
	    	 for(int i = 0; i < list.size(); ++i) {
	    		 TextView product = new TextView(getBaseContext());
	    		 product.setText(list.get(i));
	    		 layout.addView(product);
	    		 
	    	 }
	    	 setContentView(layout);
	     }
	 } // EOC
	 
} // EOF
