package com.example.gaedemo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class ShowProductsActivity extends Activity {
	public static final String TAG = "ShowProductsActivity";
	
	private Spinner spinner;
	ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//create showproducts layout first.
		// Click on layout, New->Android XML File.
		// ResourceType should be Layout.
		// Add a TextView field in it. 
		setContentView(R.layout.showproducts);
		
		// Now create a method which get query your Google App Engine via HTTP GET.
		// Make sure that method gets the data via a new thread
		// or use async task.
		EditText productsList = (EditText) findViewById(R.id.productList);
		// Update the TextView with itemsData.
		UpdateProductTask upt = new UpdateProductTask();
		upt.execute(MainActivity.PRODUCT_URI);
	}
	


	 private class UpdateProductTask extends AsyncTask<String, Void, List<String>> {
		 @Override
	     protected List<String> doInBackground(String... url) {
			 
	    	 HttpClient client = new DefaultHttpClient();
				HttpGet request = new HttpGet(url[0]);
				List<String> list = new ArrayList<String>();
				try {
					HttpResponse response = client.execute(request);
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					Log.d(TAG, data);
					JSONObject myjson;
					
					try {
						myjson = new JSONObject(data);
						JSONArray array = myjson.getJSONArray("data");
						for (int i = 0; i < array.length(); i++) {
							JSONObject obj = array.getJSONObject(i);
							list.add(obj.get("name").toString());
						}
						
					} catch (JSONException e) {

				    	Log.d(TAG, "Error in parsing JSON");
					}
					
				} catch (ClientProtocolException e) {

			    	Log.d(TAG, "ClientProtocolException while trying to connect to GAE");
				} catch (IOException e) {

					Log.d(TAG, "IOException while trying to connect to GAE");
				}
			 System.out.println("list: " + list);
	         return list;
	     }

	     protected void onPostExecute(List<String> list) {
	    	 Context context = getBaseContext();
	    	 LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	 View view = mInflater.inflate(R.layout.showproducts, null);
	    	 LinearLayout layout = (LinearLayout) view;

	    	 // Loop through the list and create a new TextView for each product
	    	 for(int i = 0; i < list.size(); ++i) {
	    		 
	    		 TextView product = new TextView(getBaseContext());
	    		 product.setText(list.get(i));
	    		 layout.addView(product);
	    	 }
	    	 setContentView(layout);
	     }
	 } // EOC
	
} // EOF
